import numpy as np
import h5py
import scipy.signal
from matplotlib import pyplot as plt
from skimage.measure import block_reduce
from scipy.signal import butter
import math as m
import os
import glob

print("test")
print("test")

def round_down(num, divisor):
    return num - (num%divisor)

def range_plus(start, stop, num_steps):
	range_size = stop-start
	step_size = float(range_size)/(num_steps-1)
	for step in range(num_steps):
		yield round(start + step*step_size, 1)

""" Definitionen von AP-Sensing """
def phase_to_strain(phase, guage_length):
	epsilon = 0.17
	n0 = 1.4682
	P11 = 0.12
	P12 = 0.27
	scale_factor = 1 - (0.5 * n0 * n0) * (P12 - (epsilon * (P11 + P12)))
	wave_length = 1550e-9
	strain = ((phase * wave_length) / (2 * 2 * np.pi * n0 * guage_length * scale_factor))
	return strain


""" Eigene Definitionen """
def pix2inch(pixel):
	return pixel / 72


def changePlotSize(x_res, y_res, subplotCountX, subplotCountY):
	plt.figure(figsize=(pix2inch(x_res * subplotCountX), pix2inch(y_res * subplotCountY)))
	plt.rcParams.update({'font.size': int(m.sqrt(x_res * y_res) * 2.25e-02)})


def butter_bandpass(lowcut, highcut, fs, order=5):
	nyq = 0.5 * fs
	low = lowcut / nyq
	high = highcut / nyq
	sos = butter(order, [low, high], btype='band', output='sos')
	return sos


def butter_bandpass_filter_abs(data, lowcut, highcut, fs, order=5):
	sos = butter_bandpass(lowcut, highcut, fs, order=order)
	y = scipy.signal.sosfilt(sos, data)
	return abs(y)


def sampleCount(seconds, fs):
	return seconds * fs


def fileSizeLimit(filepath):
	size = os.path.getsize(filepath)
	return True if sizelimit <= size else False

def extraFilter(filepath):
	exceptions = [r"F:\12-21 IAB Weimar\DAS Data\0.4mm Leckage\00 Innen nach Außen (H2O)\0000000014_2021-11-30_14.22.54.01952.hdf5"]
	if filepath in exceptions:
		return False
	else:
		return True

# und hier ein komment iom master

sizelimit = 1e8
# folderDirectory = r"/Volumes/TIMBUKTU/GESO/IAB/hdf5"
root = r"F:\12-21 IAB Weimar\DAS Data"
rootSafeTo = r"F:\12-21 IAB Weimar\Ausgaben"
filepathlist_unfilt = [os.path.join(path, name) for path, subdirs, files in os.walk(root) for name in files]
filepathlist = list(filter(fileSizeLimit, filepathlist_unfilt))
filepathlist = list(filter(extraFilter, filepathlist))
# folderDirectory = r"I:\210723\DAS\Alles\auswahl"
# folderDirectory = r"H:\TIMBUKTU Zwischenspeicher\GESO Praktikum\IAB Weimar\hdf5"
# folderDirectory = r"I:\210723\DAS\Alles"
# filepathlist = sorted(glob.glob(os.path.join(folderDirectory, '*.hdf5')))
filenamelist = [os.path.basename(path) for path in filepathlist]


def tuttiFrutti(pStart, pEnd, tStart, tEnd, vMin, vMax, verzeichnisNr, flippyFlip, xPlotRes, yPlotRes, xPlotNum, yPlotNum):

	for file in filepathlist:
		safeTo = file.replace(root, rootSafeTo)
		print(file)
		with h5py.File(file, 'r', libver='latest') as h5_file:
			repetition_frequency = h5_file['DAQ/RepetitionFrequency'][0]
			spatial_sampling = h5_file['/ProcessingServer/SpatialSampling'][0]
			shape = h5_file['DAS'].shape
			recTime = shape[0] * (1 / repetition_frequency)
			trace_count = h5_file['/Metadata/TraceCount'][0]
			points = h5_file['/DAQ/SpatialPoints'][0]
			spatial_sampling_points = h5_file['/ProcessingServer/SpatialSamplingPoints'][0]
			gauge_length = h5_file['/ProcessingServer/GaugeLength'][0]
			zeroOffset = h5_file['/Metadata/ZeroOffset'][0]
			
			sampleStart = int(repetition_frequency * tStart)
			sampleEnd = int(repetition_frequency * tEnd)
			
			data_dphase = h5_file.get('DAS')[sampleStart:sampleEnd, pStart:pEnd]
		data_phase = np.cumsum(data_dphase, axis=0) * (np.pi / 2 ** 15)
		data_strain = phase_to_strain(data_phase, gauge_length)
		data_strain = data_strain.T
		
		# data_strain = sinWave()
		'''
		dataDicDAS = {
			"3000 - 5000 Hz": data_strain.copy(),
			"Spektrogramm": data_strain.copy()
		}
		'''

		dataDicDAS = {
			"100 - 10000 Hz": data_strain.copy(),
			"4 - 250 Hz": data_strain.copy(),
			"250 - 4000 Hz": data_strain.copy(),
			"4000 - 7000 Hz": data_strain.copy(),
			"7000 - 10000 Hz": data_strain.copy(),
			"Spektrogramm": data_strain.copy()
		}
		# dataDicDAS = {
		# 	"4 - 500 Hz": data_strain.copy(),
		# 	"500 - 1000 Hz": data_strain.copy(),
		# 	"1000 - 1500 Hz": data_strain.copy(),
		# 	"1500 - 2000 Hz": data_strain.copy(),
		# 	"2500 - 3000 Hz": data_strain.copy(),
		# 	"3000 - 3500 Hz": data_strain.copy(),
		# 	"3500 - 4000 Hz": data_strain.copy(),
		# 	"4000 - 4500 Hz": data_strain.copy(),
		# 	"4500 - 5000 Hz": data_strain.copy(),
		# 	"5000 - 5500 Hz": data_strain.copy(),
		# 	"5500 - 6000 Hz": data_strain.copy(),
		# 	"6000 - 6500 Hz": data_strain.copy(),
		# 	"6500 - 7000 Hz": data_strain.copy(),
		# 	"7000 - 7500 Hz": data_strain.copy(),
		# 	"7500 - 8000 Hz": data_strain.copy(),
		# 	"8000 - 8500 Hz": data_strain.copy(),
		# 	"8500 - 9000 Hz": data_strain.copy(),
		# 	"9000 - 9500 Hz": data_strain.copy(),
		# 	"9500 - 10000 Hz": data_strain.copy(),
		# 	"Spektrogramm": data_strain.copy()
		# }

		for key in dataDicDAS.keys():
			if key != "Spektrogramm":
				bananasplit = key.split(" ", 3)
				dataDicDAS[key] = butter_bandpass_filter_abs(dataDicDAS.get(key), int(bananasplit[0]), (float(bananasplit[2]) - 0.1), repetition_frequency, order=10)
			if key == "Spektrogramm":
				dataDicDAS[key] = butter_bandpass_filter_abs(dataDicDAS.get(key), 2, 9999.9, repetition_frequency, order=10)

		scaleFactor = int(data_strain.shape[1] / 1280)
		# scaleFactor = int(data_strain.shape[1] / 1280)
		
		for key, value in dataDicDAS.items():
			if key != "Spektrogramm":
				# dataDicDAS[key] = avgResult = np.average(value.reshape(-1, int(len(value)/scaleFactor)), axis=1)
				dataDicDAS[key] = block_reduce(value, block_size=(1, scaleFactor), func=np.mean, cval=np.mean(value)).T
				dataDicDAS[key] = np.fliplr(dataDicDAS[key])
				# dataDicDAS[key] = np.swapaxes(block_reduce(value, block_size=(1, scaleFactor), func=np.mean, cval=np.mean(value)), 0, 1)
				# dataDicDAS[key] = np.nan_to_num(value)
				# dataDicDAS[key] = np.flipud(block_reduce(value, block_size=(scaleFactor, 1), func=np.mean, cval=np.mean(value)))
				print(dataDicDAS[key].shape)
				
				# if flippyFlip:
				#    dataDicDAS[key] = np.flipud(dataDicDAS[key])
		
		changePlotSize(1280, 720, 5, 1)
		i = 0
		for key, value in dataDicDAS.items():
			i += 1
			if key != "Spektrogramm":
				plt.subplot(1, 5, i)
				print(value.shape)
				plt.xlabel('Messpunkt Nr.')
				plt.ylabel('Zeit [s]')
				# plt.xlim(int(sampleStart/scaleFactor), int(sampleEnd/scaleFactor))
				if flippyFlip:
					plt.yticks(list(range(0, (pEnd - pStart))), list(reversed(range(pStart, pEnd))))
				# if not flippyFlip:
					# print("here")
					# plt.xticks(list(range(0, (pEnd - pStart))), list((range(pStart, pEnd))))
					# plt.yticks(np.arange(0, (pEnd - pStart), 39.463299131807425), (np.arange(0, (pEnd - pStart), 39.463299131807425) * spatial_sampling) - 81.848)
					# crezyTicks = np.arange(round_down(pStart * spatial_sampling - 0, 5), round_down(pEnd * spatial_sampling - 0, 5), 2)
					# plt.xticks((np.arange(round_down(pStart * spatial_sampling - 0, 5), round_down(pEnd * spatial_sampling - 0, 5), 2) / spatial_sampling) - (crezyTicks / spatial_sampling).min(), np.arange(round_down(pStart * spatial_sampling - 0, 5), round_down(pEnd * spatial_sampling - 0, 5), 2))
				# if key == "4 - 250 Hz":
				# 	plt.imshow(value, cmap=plt.cm.turbo, aspect='auto', interpolation='none', vmin=0, vmax=1.2e-8)
				# if key == "250 - 3000 Hz":
				# 	plt.imshow(value, cmap=plt.cm.turbo, aspect='auto', interpolation='none', vmin=0, vmax=5e-9)
				# if key == "3000 - 6000 Hz":
				# 	plt.imshow(value, cmap=plt.cm.turbo, aspect='auto', interpolation='none', vmin=0, vmax=2e-9)
				# if key == "6000 - 10000 Hz":
				# 	plt.imshow(value, cmap=plt.cm.turbo, aspect='auto', interpolation='none', vmin=0, vmax=2e-10)
				plt.imshow(value, cmap=plt.cm.turbo, aspect='auto', interpolation='none', vmin=0, vmax=2e-9)
				plt.colorbar()
				plt.axvline(36.181 / spatial_sampling, c='white', lw=2.5)
				plt.axvline(42.151 / spatial_sampling, c='white', lw=2.5)
				plt.axvline(54.142 / spatial_sampling, c='white', lw=2.5)
				plt.axvline(60.312 / spatial_sampling, c='white', lw=2.5)
				plt.axvline(116.917 / spatial_sampling, c='white', lw=2.5)
				plt.axvline(123.183 / spatial_sampling, c='white', lw=2.5)
				plt.axvline(135.174 / spatial_sampling, c='white', lw=2.5)
				plt.axvline(141.250 / spatial_sampling, c='white', lw=2.5)
				locs, labels = plt.yticks()
				plt.yticks(locs[1:-1], list(np.linspace(start=tStart, stop=tEnd, num=(len(locs) - 2), dtype=int)))
				plt.title('{}'.format(key))
				plt.suptitle('{}'.format(file), y=0.035)
				plt.tight_layout(pad=2)
		plt.savefig('{}/{}_Wasserfalldiagramm_time{:02d}-{:02d}_point_{:04d}-{:04d}.png'.format(os.path.dirname(safeTo), os.path.basename(file), int(tStart), int(tEnd), pStart, pEnd), facecolor='w')
		plt.close()
		print("safed")
		# plt.show()
		# for key, value in dataDicDAS.items():
		#    if key == "Spektrogramm":
		#       changePlotSize(1280, 720, 1, 1)
		#       plt.subplot()
		#       print(value[2,:].shape)
		#       f, t, Sxx = signal.spectrogram(value[2,:], repetition_frequency)
		#       # plt.pcolormesh(t, f, Sxx, cmap=plt.cm.turbo, shading='auto')
		#       plt.pcolormesh(t, f, Sxx, cmap=plt.cm.turbo, shading='auto', vmax=1e-22, vmin=0)
		#       plt.title('{} | {:.1f} m − {:.1f} m | Messstrecke {}'.format(key,(pStart * spatial_sampling), (pEnd * spatial_sampling), verzeichnisNr))
		#       plt.ylabel('Frequenz [Hz]')
		#       plt.xlabel('Zeit [s]')
		#       plt.colorbar()
		#       plt.tight_layout(pad=1)
		# plt.savefig('{}/{}_Spektrogramm_time{:02d}-{:02d}_point_{:04d}-{:04d}.png'.format(safeTo, os.path.basename(file), int(tStart), int(tEnd), pStart, pEnd), facecolor='w')
		# plt.show()
		
		globals().update(locals())

#
# tuttiFrutti(28, 33, 0, 60, 0, 1.8 * 1e-9, 1, False, 1280, 720, 4, 1)  # 1 (1)
# tuttiFrutti(42, 47, 0, 60, 0, 1.8 * 1e-9, 2, True, 1280, 720, 4, 1)  # 2 (2)
# tuttiFrutti(91, 96, 0, 60, 0, 1.8 * 1e-9, 3, False, 1280, 720, 4, 1)  # 3 (3)
# tuttiFrutti(105, 110, 0, 60, 0, 1.8 * 1e-9, 4, True, 1280, 720, 4, 1)  # 4 (4)
# tuttiFrutti(0, 5, 0, 60, 0, 1, 4, True, 1280, 720, 4, 1)  # 4 (4)
# tuttiFrutti(130, 160, 0, 30, 0, 1.8 * 1e-9, 1, False, 1280, 720, 6, 7)
# tuttiFrutti(73, 500, 0, 1, 0, 1.8 * 1e-9, 1, False, 1280, 720, 6, 7)
# tuttiFrutti(73, 1010, 0, 30, 0, 1.8 * 1e-9, 1, False, 1280, 720, 1, 1)
# tuttiFrutti(130, 160, 0, 60, 0, 1.8 * 1e-9, 1, False, 1280, 720, 1, 1)
# tuttiFrutti(980, 1010, 0, 30, 0, 1.8 * 1e-9, 1, False, 1280, 720, 1, 1)
tuttiFrutti(0, 130, 0, 60, 0, 1.8 * 1e-9, 1, False, 1280, 720, 5, 1)
# tuttiFrutti(73, 500, 0, 30, 0, 1.8 * 1e-9, 1, False, 1280, 720, 6, 7)
# tuttiFrutti(500, 1011, 0, 30, 0, 1.8 * 1e-9, 1, False, 1280, 720, 6, 7)